<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
//        $image = $this->faker->image('public/images',640,480, null, false);
//        dd($image);
        return [
            'name'=>$this->faker->name(300),
            'description'=>$this->faker->paragraph(),
            'image_path'=>$this->faker->file($sourceDir = 'public/sample_images', $targetDir = 'public/images',false),
//            'image_path'=>$this->faker->image('public/images',640,480, null, false),
            'price'=>$this->faker->randomFloat(2,0,100),

        ];
    }
}
