@extends("layouts.app")

@section("content")
    <style>
        .container{
            max-width:540px;
        }
        .form-group{
            margin: 10px;
        }
    </style>
    <section style="background-color: #eee;">
        <div class="container py-5">
            {{--            metthod = "POST" enctype = "multipart/form-data"--}}
            <form action = "/products/{{$product->id}}" method = "POST"  enctype = "multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group" >
                    <label for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name = "name" value = {{$product->name}}>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Price($)</label>
                    <input type="number" class="form-control" id="exampleFormControlInput2" name = "price" value = {{$product->price}}>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name = "description" >{{$product->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Image</label><br/>
                    <input name = "image" type="file" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-primary mt-2" type="submit">Update</button>
                </div>
            </form>

        </div>
    </section>
@endsection
